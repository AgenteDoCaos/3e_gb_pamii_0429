export class Contrato_Proprietario{
    id: number
    id_imovel: number
    id_imobiliaria: number
    data_inicial: string
    data_termino: string
    aluguel: number
    venda: number
}