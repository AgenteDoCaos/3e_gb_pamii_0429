export class Imovel{
    private _endereco: String;
    private _bairro: String;
    private  _cep: String;
    private _cidade: String;
    private _uf: String;
    private _qtdQuartos: number;
    private _qtdSalas: number;
    private _qtdBanheiros: number;
    private _qtdCozinhas: number;
    public   andares: number;
    private _complemento: string;
    private _valorVenal: number;
    private _valorLocacao: number;
    private _isLocavel: boolean;
    private _isVenal: boolean;
    private _situacao: string;

constructor(){
    this._endereco = "endereço não informado ou incorreto";
    this._bairro = "bairro não informado ou incorreto";
    this._cep = "cep não informado ou incorreto";
    this._cidade = "cidade não informada ou incorreta";
    this._uf = "uf não informado ou incorreto";
    this._qtdBanheiros = 0;
    this._qtdCozinhas = 0;
    this._qtdQuartos = 0;
    this._qtdSalas = 0;
    this.andares = 1;
    this._valorVenal = 1;
    this._valorLocacao = 1;
    this._isLocavel = false;
    this._isVenal = true;
    this._complemento = "sem complemento";
    this._situacao = "situação estável";

}

//-----------------------------------------------------------//

public set endereco(endereco: String){

    if(endereco.trim().length>5){
    
    this.endereco = endereco;
    
    }
    
    }
    
    public get endereco(): String{
    
    return this._endereco;
    
    }

//-----------------------------------------------------------//

public set bairro(bairro: String){

    if(bairro.trim().length>4){
    
    this.bairro = bairro;
    
    }
    
    }
    
    public get bairro(): String{
    
    return this._bairro;
    
    }
    //-----------------------------------------------------//
    public set cidade(cidade: String){

        if(cidade.trim().length>5){
        
        this.cidade = cidade;
        
        }
        
        }
        
        public get cidade(): String{
        
        return this._cidade;
        
        }
    
    //-----------------------------------------------------------//
    public set cep(cep: String){

        if(cep.trim().length>7){
        
        this.cep = cep;
        
        }
        
        }
        
        public get cep(): String{
        
        return this._cep;
        
        }
    
    //-----------------------------------------------------------//

    public set uf(uf: String){

        if(uf.trim().length>3){
        
        this.uf = uf;
        
        }
        
        }
        
        public get uf(): String{
        
        return this._uf;
        
        }
    
    //-----------------------------------------------------------//

    public set complemento(complemento: String){

        if(complemento.trim().length>2){
        
        this.complemento = complemento;
        
        }
        
        }
        
        public get complemento(): String{
        
        return this._complemento;
        
        }
    
    //-----------------------------------------------------------//

    public set situacao(situacao: String){

        if(situacao.trim().length>4){
        
        this.situacao = situacao;
        
        }
        
        }
        
        public get situacao(): String{
        
        return this._situacao;
        
        }
    
    //-----------------------------------------------------------//

    public exibirDados(): string{

        return 'Endereço: $(this._endereco) Cidade: $(this._cidade) Bairro: $(this._bairro) cep: $(this._cep) uf: $(this._uf) quantidade de quartos: $(this._qtdQuartos) quantidade de salas: $(this._qtdSalas) quantidade de banheiros: $(this._qtdBanheiros) quantidade de cozinhas: $(this._qtdCozinhas) quantidade de andares: $(this.andares) complemento: $(this._complemento) situação: $(this._situacao) valor da Venda: $(this._valorVenal) valor de locação: $(this._valorLocacao) é locavel ?: $(this._isLocavel) esta a venda ?: $(this._isVenal)';
        
        }
}