export class Cliente{
    id: number
    sexo: string
    data_nascimento: string
    data_cadastro: string
    orientacao: string
    nome_social: string
    raca: string
    deficiencia: string
    descricao_deficiencia: string
}