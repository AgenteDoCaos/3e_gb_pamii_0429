export class Contrato_Cliente{
    id: number
    id_cliente: number
    id_imobiliaria: number
    data_inicio: string
    data_termino: string
    valor_combinado: number
    tipo: string
    fiador: string
    ativo: string
}