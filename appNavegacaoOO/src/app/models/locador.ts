export class Locador{
    private _nome: String;
    private _telefone: String;
    private _dataNascimento: Date;
    private _email: String;
    private _fone: String;
    private _sexo: String;
    private _rendaMensal: number;
    private _nomeFiador: String;
    constructor(){
        this._nome = this.nome;
        this._telefone = this.telefone;
        this._dataNascimento = this.dataNascimento;
        this._email = this.email;
        this._fone = this.fone;
        this._sexo = this.sexo;
        this._rendaMensal = this.rendaMensal;
        this._nomeFiador = this.nomeFiador;
    
    }
    public set nome(nome: String){

        if(nome.trim().length>2){
        
        this.nome = nome;
        
        }
        
        }
        
        public get nome(): String{
        
        return this._nome;
        
        }
    public set telefone(telefone: String){

        if(telefone.trim().length>11){
            
        this.telefone = telefone;
            
        }
            
        }
            
        public get telefone(): String{
            
        return this._telefone;
            
        }
    public set dataNascimento(dataNascimento: Date){
  
        this.dataNascimento = dataNascimento;
                
                
        }
                
        public get data(): Date{
                
        return this._dataNascimento;
                
        }
    public set email(email: String){

        if(email.trim().length>10){
            
        this.email = email;
            
        }
            
        }
            
        public get email(): String{
            
        return this._email;
            
        }
    public set fone (fone: String){

        if(fone.trim().length>2){
                
        this.fone = fone;
                
        }
                
        }
                
        public get fone(): String{
                
        return this._fone;
                
        }
    public set sexo(sexo: String){

        if(sexo.trim().length>2){
                    
        this.sexo = sexo;
                    
        }
                    
        }
                    
        public get sexo(): String{
                    
        return this._sexo;
                    
        }
    public set rendaMensal(rendaMensal: number){


                        
        this.rendaMensal = rendaMensal;
                        
                        
        }
                        
        public get rendaMensal(): number{
                        
        return this._rendaMensal;
                        
        }
    public set nomeFiador(nomeFiador: String){

        if(this.nomeFiador.trim().length>2){
                        
        this.nomeFiador = nomeFiador;
                        
        }
                        
        }
                        
        public get nomeFiador(): String{
                        
        return this._nomeFiador;
                        
        }
}
