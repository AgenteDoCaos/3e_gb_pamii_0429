import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { CadUsuariosPageRoutingModule } from './cad-usuarios-routing.module';

import { CadUsuarioPage } from './cad-usuarios.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    CadUsuariosPageRoutingModule
  ],
  declarations: [CadUsuarioPage]
})
export class CadUsuariosPageModule {}
