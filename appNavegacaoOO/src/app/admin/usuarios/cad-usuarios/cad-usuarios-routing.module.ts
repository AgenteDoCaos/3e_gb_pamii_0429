import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CadUsuarioPage } from './cad-usuarios.page';

const routes: Routes = [
  {
    path: '',
    component: CadUsuarioPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CadUsuariosPageRoutingModule {}
