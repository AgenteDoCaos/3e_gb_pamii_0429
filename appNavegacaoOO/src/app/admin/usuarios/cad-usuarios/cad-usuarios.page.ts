import { Component, OnInit } from '@angular/core';
import { usuario } from 'src/app/models/usuario';
import { UsuariosService } from 'src/app/services/usuarios/UsuariosService';


@Component({
  selector: 'app-cad-usuario',
})
export class CadUsuarioPage implements OnInit {
  usuario: usuario

  constructor(
    private readonly usuariosService: UsuariosService
  ) {
    this.usuario = new usuario()
  }

  ngOnInit() {
  }

  salvar(): void {
    this.usuariosService.cadastrarUsuario(this.usuario)
    .subscribe({
      next: (resultado) => {
        console.log(resultado)
      },
      error: (erro) => {
        console.error(erro)
      }
    })
  }
}

