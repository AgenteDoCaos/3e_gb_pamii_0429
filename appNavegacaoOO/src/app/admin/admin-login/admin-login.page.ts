import { Component, OnInit } from '@angular/core';
import { Route } from '@angular/router';
import { usuario } from 'src/app/models/usuario';
import { UsuariosService } from 'src/app/services/usuarios/UsuariosService';


@Component({
  selector: 'app-admin-login',
  templateUrl: './admin-login.page.html',
  styleUrls: ['./admin-login.page.scss'],
})
export class AdminLoginPage implements OnInit {
  usuario: usuario
  erro: any
  mensagem: string

  constructor(
    private readonly usuariosService: UsuariosService,
    private readonly router: Route
  ) {
    this.usuario = new usuario()
  }

  ngOnInit() {
  }

  logar(): void {
    this.usuariosService.logar(this.usuario)
    .subscribe({
      next: (resultado) => {
        console.log(resultado)
        this.erro = null
      },
      error: (erro) => {
        this.erro = erro
        this.mensagem = this.erro.error.mensagem
        console.error(erro)
      }
    })
  }
}
