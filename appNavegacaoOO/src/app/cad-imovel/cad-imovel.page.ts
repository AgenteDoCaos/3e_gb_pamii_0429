import { Component, OnInit } from "@angular/core";
import { Imovel } from "../models/imovel";

@Component({
    selector: 'app-cad-imovel',
    templateUrl: 'cad-imovel.page.html',
    styleUrls: ['cad-imovel.page.scss'],
})

export class CadImovelPage implements OnInit{
     itupeva: Imovel = new Imovel();
    constructor() {

       this.itupeva.cidade = "Rhuan de Almeida Oliveira"
       this.itupeva.bairro = "azulado";
       this.itupeva.endereco = "rua que sobe e desce";
       this.itupeva.cep = "12154787";
       this.itupeva.uf = "799426";
       this.itupeva.complemento = "primeiro cadastrado";
       this.itupeva.situacao = "complicada";
       this.exibirDadosObjetos(this.itupeva)
     }

     exibirDadosObjetos(imovel: Imovel){
        console.log(imovel.exibirDados)
      }

    ngOnInit() {
    }

}
