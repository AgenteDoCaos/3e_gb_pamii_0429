import { Component } from '@angular/core';
import { UsuariosService } from "../services/usuarios/UsuariosService";

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {
  telas = [

  ];


 
  constructor(
    private usuariosService: UsuariosService
  ) {
    this.usuariosService.buscarUsuarios()
    .subscribe(
      (data) => {
        console.log(data)
      }, (error) => {
        console.error(error)
      }
    )
    this.usuariosService.buscarUsuariosCompletos()
    .subscribe(
      (data) => {
        console.log(data)
      }, (error) => {
        console.error(error)
      }
    )
}

}
