import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { CadLocadorsPageRoutingModule } from './cad-locadors-routing.module';

import { CadLocadorsPage } from './cad-locadors.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    CadLocadorsPageRoutingModule
  ],
  declarations: [CadLocadorsPage]
})
export class CadLocadorsPageModule {}
