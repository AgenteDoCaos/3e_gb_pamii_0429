import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CadLocadorsPage } from './cad-locadors.page';

const routes: Routes = [
  {
    path: '',
    component: CadLocadorsPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CadLocadorsPageRoutingModule {}
