import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'home',
    pathMatch: 'full'
  },
  {
    path: 'home',
    loadChildren: () => import('./home/home.module').then( m => m.HomePageModule)
  },
  
  {
    path: 'cad-imovel',
    loadChildren: () => import('./cad-imovel/cad-imovel.module').then( m => m.CadImovelPageModule)
  },
  {
    path: 'cad-imobiliaria',
    loadChildren: () => import('./cad-imobiliaria/cad-imobiliaria.module').then( m => m.CadImobiliariaPageModule)
  },
  {
    path: 'cad-proprietario',
    loadChildren: () => import('./cad-proprietario/cad-proprietario.module').then( m => m.CadProprietarioPageModule)
  },
  {
    path: 'cad-locadors',
    loadChildren: () => import('./cad-locadors/cad-locadors.module').then( m => m.CadLocadorsPageModule)
  },
  {
    path: 'teste',
    loadChildren: () => import('./teste/teste.module').then( m => m.TestePageModule)
  },
  {
    path: 'cad-usuario',
    loadChildren: () => import('./cad-usuario/cad-usuario.module').then( m => m.CadUsuarioPageModule)
  },
  {
    path: 'dashboard',
    loadChildren: () => import('./admin/dashboard/dashboard.module').then( m => m.DashboardPageModule)
  },
  {
    path: 'admin-login',
    loadChildren: () => import('./admin/admin-login/admin-login.module').then( m => m.AdminLoginPageModule)
  },
  {
    path: 'usuarios',
    loadChildren: () => import('./admin/usuarios/usuarios.module').then( m => m.UsuariosPageModule)
  },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
