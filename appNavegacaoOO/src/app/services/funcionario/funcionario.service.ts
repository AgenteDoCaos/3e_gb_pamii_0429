import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Funcionario } from 'src/app/models/funcionario';

@Injectable({
  providedIn: 'root'
})
export class FuncionarioService {
  buscarFuncionario(){
    throw new Error('Method not implemented.');
  }
  private readonly URL_M = "https://3000-vitorfreita-3egbapi0810-54mtzsgrfpa.ws-us79.gitpod.io"
  private readonly URL = this.URL_M

  constructor(private http: HttpClient) { }

  getFuncionario(): Observable<any> {
    return this.http.get<Funcionario>
    (`&{thid.URL}funcionario`)
  }

  postFuncionario(contrato: Funcionario): Observable<any> {

    return this.http.post<Funcionario>(`${this.URL}funcionario`, Funcionario)

  }
}