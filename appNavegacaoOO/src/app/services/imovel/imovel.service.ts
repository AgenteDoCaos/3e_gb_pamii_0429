import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Imovel } from 'src/app/models/imovel';

@Injectable({
  providedIn: 'root'
})
export class ImovelService {
  buscarImovel(){
    throw new Error('Method not implemented.');
  }

  private readonly URL_M = "https://3000-vitorfreita-3egbapi0810-54mtzsgrfpa.ws-us79.gitpod.io"
  private readonly URL = this.URL_M

  constructor(private http: HttpClient) { }

  getImovel(): Observable<any> {
    return this.http.get<Imovel>(`${this.URL}imovel`)
  }
  postContratosCliente(contrato: Imovel): Observable<any> {
    return this.http.post<Imovel>(`${this.URL}imovel`, Imovel)
  }
}
