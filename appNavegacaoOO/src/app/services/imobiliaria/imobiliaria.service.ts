import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { imobiliaria } from 'src/app/models/imobiliaria';

@Injectable({
  providedIn: 'root'
})
export class ImobiliariaService {
  buscarImobiliaria(){
    throw new Error('Method not implemented.');
  }
  private readonly URL_M = "https://3000-vitorfreita-3egbapi0810-54mtzsgrfpa.ws-us79.gitpod.io"
  private readonly URL = this.URL_M

  constructor(private http: HttpClient) { }

  getImobiliaria(): Observable<any> {
    return this.http.get<imobiliaria>
    (`&{thid.URL}imobiliaria`)
  }

  postImobiliaria(contrato: imobiliaria): Observable<any> {

    return this.http.post<imobiliaria>(`${this.URL}imobiliaria`, imobiliaria)

  }
}
