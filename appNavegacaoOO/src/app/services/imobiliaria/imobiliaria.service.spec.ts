import { TestBed } from '@angular/core/testing';

import { ImobiliariaService } from './imobiliaria.service';

describe('ImobiliariaService', () => {
  let service: ImobiliariaService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ImobiliariaService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
