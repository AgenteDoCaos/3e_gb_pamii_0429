import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { usuario } from 'src/app/models/usuario';


@Injectable({
  providedIn: 'root'
})
export class UsuariosService {
  private readonly URL_J = "https://3000-profdanilom-3egbapi0810-gmeqv1cwws9.ws-us78.gitpod.io/";
  private readonly URL_M = "https://3000-vitorfreita-3egbapi0810-54mtzsgrfpa.ws-us79.gitpod.io/";
  private readonly URL = this.URL_M;

  constructor(
    private httpClient: HttpClient
  ) { }

  logar(Usuario: usuario): Observable<any> {
    return this.httpClient.post<any>(`${this.URL}usuario/logar`, Usuario);
  }

  cadastrarUsuario(Usuario: usuario): Observable<any> {
    return this.httpClient.post<any>(`${this.URL}usuario`, Usuario);
  }

  buscarUsuarios(): Observable<any> {
    return this.httpClient.get<any>(`${this.URL}usuarios`);
  }

  buscarUsuariosCompletos(): Observable<any> {
    return this.httpClient.get<any>(`${this.URL}usuarios/completos`);
  }
}
