import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Contrato_Proprietario } from 'src/app/models/contrato_proprietario';

@Injectable({

  providedIn: 'root'

})

export class ContratoProprietarioService {

  buscarContrato_Proprietario() {
    throw new Error('Method not implemented.');
  }

  private readonly URL_V = "https://3000-vitorfreita-3egbapi0810-z71f60sf1zb.ws-us79.gitpod.io/"
  private readonly URL = this.URL_V

  constructor(private http: HttpClient) { }
  getContrato_Proprietario(): Observable<any> {
    return this.http.get<Contrato_Proprietario>(`${this.URL}Contrato_Proprietario`)
  }



  postContratosProprietario(contrato: Contrato_Proprietario): Observable<any> {

    return this.http.post<Contrato_Proprietario>(`${this.URL}Contrato_Proprietario`, Contrato_Proprietario)

  }

}