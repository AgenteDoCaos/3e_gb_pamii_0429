import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Cliente } from 'src/app/models/cliente';

@Injectable({

  providedIn: 'root'

})

export class ClienteService {

  buscarCliente() {
    throw new Error('Method not implemented.');
  }

  private readonly URL_V = "https://3000-vitorfreita-3egbapi0810-z71f60sf1zb.ws-us79.gitpod.io/"
  private readonly URL = this.URL_V

  constructor(private http: HttpClient) { }
  getCliente(): Observable<any> {
    return this.http.get<Cliente>(`${this.URL}clientes`)
  }



  postContratosCliente(contrato: Cliente): Observable<any> {

    return this.http.post<Cliente>(`${this.URL}cliente`, Cliente)

  }

}